import BlogList from './BlogList'
import useFetch from '../controller/useFetch'
const Home = () => {
    const {data: blogs, load, error} = useFetch('http://localhost:8000/blogs')

    return (  
        <div className="home">
            {error && <div>{ error }</div>}
            {load && <div>Loading . . . </div>}
            {blogs && <BlogList blogs={blogs} title='All Blogs' />}
        </div>
    );
}
 
export default Home;
