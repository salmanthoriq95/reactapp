import { useHistory, useParams } from "react-router";
import useFetch from "../controller/useFetch";

const ShowBlog = () => {
    const {id} = useParams();
    const {data: blog, error, load} = useFetch('http://localhost:8000/blogs/' + id);
    const history = useHistory();
    
    const clickHandler = () => {
        fetch('http://localhost:8000/blogs/' + blog.id, {
            method: 'DELETE',
        }).then(()=>{
            history.push('/');
            console.log('blog deleted')
        })
    }

    return (
        <div className="show-blog">
            {error && <div>{ error }</div>}
            {load && <div>Loading . . . </div>}
            {blog && (
                <article>
                    <h2>{blog.title}</h2>
                    <p><strong>Written by : {blog.author}</strong></p>
                    <div dangerouslySetInnerHTML={{__html:blog.body}} />
                    <button onClick={clickHandler}>Delete</button>
                </article>
            )}
        </div>
    );
}
 
export default ShowBlog;